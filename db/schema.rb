# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150406155858) do

  create_table "asds", force: :cascade do |t|
    t.date     "a"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cabinas", force: :cascade do |t|
    t.integer  "votacion_id", limit: 4
    t.integer  "number",      limit: 4
    t.string   "ip",          limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "cabinas", ["votacion_id"], name: "index_cabinas_on_votacion_id", using: :btree

  create_table "personas", force: :cascade do |t|
    t.integer  "rut_int",                      limit: 4
    t.string   "nombre",                       limit: 255
    t.string   "empresa",                      limit: 255
    t.boolean  "es_socio",                     limit: 1,   default: true,                  null: false
    t.boolean  "puede_ser_representante",      limit: 1,   default: true,                  null: false
    t.date     "fecha_ingreso"
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
    t.integer  "estado",                       limit: 4,   default: 0,                     null: false
    t.integer  "votos_recibidos_electronicos", limit: 4,   default: 0,                     null: false
    t.integer  "votos_recibidos_manuales",     limit: 4,   default: 0,                     null: false
    t.boolean  "ha_votado",                    limit: 1,   default: false,                 null: false
    t.integer  "tipo_voto",                    limit: 4,   default: 0,                     null: false
    t.integer  "votacion_id",                  limit: 4
    t.datetime "hora_asistencia",                          default: '2015-04-05 22:39:29'
  end

  add_index "personas", ["rut_int", "votacion_id"], name: "index_personas_on_rut_int_and_votacion_id", unique: true, using: :btree

  create_table "poders", force: :cascade do |t|
    t.integer  "poderante_id",       limit: 4
    t.integer  "apoderado_id",       limit: 4
    t.integer  "votacion_id",        limit: 4
    t.integer  "relacion_partes",    limit: 4, default: 0, null: false
    t.date     "fecha_otorgamiento"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "tiempo_de_votacions", force: :cascade do |t|
    t.integer  "persona_id",  limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "votacion_id", limit: 4
    t.integer  "cabina_id",   limit: 4
  end

  add_index "tiempo_de_votacions", ["persona_id"], name: "index_tiempo_de_votacions_on_persona_id", using: :btree
  add_index "tiempo_de_votacions", ["votacion_id"], name: "index_tiempo_de_votacions_on_votacion_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "username",        limit: 255
    t.string   "password_digest", limit: 255
    t.integer  "tipo",            limit: 4,   default: 1,    null: false
    t.boolean  "auth",            limit: 1,   default: true, null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "votacions", force: :cascade do |t|
    t.date     "fecha_realizacion"
    t.string   "nombre",                                  limit: 255
    t.integer  "estado",                                  limit: 4,   default: 0, null: false
    t.integer  "n_ganadores",                             limit: 4,   default: 1, null: false
    t.integer  "max_representacion",                      limit: 4,   default: 3, null: false
    t.integer  "n_blancos",                               limit: 4,   default: 0, null: false
    t.integer  "n_nulos_forma",                           limit: 4,   default: 0, null: false
    t.integer  "n_nulos_plazo",                           limit: 4,   default: 0, null: false
    t.integer  "max_minutos_voto",                        limit: 4,   default: 5, null: false
    t.integer  "n_cabinas",                               limit: 4,   default: 1, null: false
    t.integer  "sig_indice_cabina",                       limit: 4,   default: 0, null: false
    t.integer  "votos_nulos_electronicos",                limit: 4,   default: 0, null: false
    t.integer  "votos_blancos_electronicos",              limit: 4,   default: 0, null: false
    t.integer  "votos_nulos_manuales",                    limit: 4,   default: 0, null: false
    t.integer  "votos_blancos_manuales",                  limit: 4,   default: 0, null: false
    t.integer  "total_votos_electronicos_segun_votantes", limit: 4,   default: 0, null: false
    t.integer  "total_votos_manuales_segun_votantes",     limit: 4,   default: 0, null: false
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.integer  "estado_candidatura",                      limit: 4,   default: 0, null: false
    t.integer  "votos_recibidos",                         limit: 4,   default: 0, null: false
    t.integer  "votacion_id",                             limit: 4
  end

  add_foreign_key "tiempo_de_votacions", "personas"
end
