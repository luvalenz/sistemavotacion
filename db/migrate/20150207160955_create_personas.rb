class CreatePersonas < ActiveRecord::Migration
  def change
    create_table :personas do |t|
      t.integer :rut_int
      t.string :nombre
      t.string :empresa
      t.boolean :es_socio, null: false, default: true
      t.boolean :puede_ser_representante, null: false, default: true
      t.date :fecha_ingreso
      t.timestamps null: false
      t.integer :estado, null: false, default: 0
      t.integer :votos_recibidos_electronicos, null: false, default: 0
      t.integer :votos_recibidos_manuales, null: false, default: 0
      t.boolean :ha_votado, null: false, default: false
      t.integer :tipo_voto, null: false, default: 0
      t.belongs_to :votacion
    end
    remove_index :accounts, :column
    add_index(:personas, [:rut_int,:votacion_id], unique: true)
  end
end
