class CreateVotacions < ActiveRecord::Migration
  def change
    create_table :votacions do |t|
      t.date :fecha_realizacion
      t.string :nombre
      t.integer :estado, null: false, default: 0
      t.integer :n_ganadores, null: false, default: 1
      t.integer :max_representacion, null: false, default: 3
      t.integer :n_blancos, null: false, default: 0
      t.integer :n_nulos_forma, null: false, default: 0
      t.integer :n_nulos_plazo, null: false, default: 0
      t.integer :max_minutos_voto, null: false, default: 5
      t.integer :n_cabinas, null: false, default: 1
      t.integer :sig_indice_cabina, null:false, default: 0
      t.integer :votos_nulos_electronicos, null:false, default: 0
      t.integer :votos_blancos_electronicos, null:false, default: 0
      t.integer :votos_nulos_manuales, null:false, default: 0
      t.integer :votos_blancos_manuales, null:false, default: 0
      t.integer :total_votos_electronicos_segun_votantes, null: false, default: 0
      t.integer :total_votos_manuales_segun_votantes, null: false, default: 0
      t.timestamps null: false
      t.integer :estado_candidatura, null: false, default: 0
      t.integer :votos_recibidos, null: false, default: 0
      t.belongs_to :votacion
    end
  end
end
