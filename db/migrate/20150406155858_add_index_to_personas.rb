class AddIndexToPersonas < ActiveRecord::Migration
  def change
    remove_index :personas, column: :rut_int
    add_index(:personas, [:rut_int,:votacion_id], unique: true)
  end
end
