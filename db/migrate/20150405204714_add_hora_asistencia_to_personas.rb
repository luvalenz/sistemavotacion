class AddHoraAsistenciaToPersonas < ActiveRecord::Migration
  def change
    add_column :personas, :hora_asistencia, :datetime, default: Time.now
  end
end
