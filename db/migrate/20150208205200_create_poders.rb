class CreatePoders < ActiveRecord::Migration
  def change
    create_table :poders do |t|
      t.belongs_to :poderante
      t.belongs_to :apoderado
      t.belongs_to :votacion
      t.integer :relacion_partes, null: false, default: 0
      t.date :fecha_otorgamiento
      t.timestamps null: false
    end
  end
end
