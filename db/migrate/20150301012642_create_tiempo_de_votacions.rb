class CreateTiempoDeVotacions < ActiveRecord::Migration
  def change
    create_table :tiempo_de_votacions do |t|
      t.belongs_to :persona, index: true
      t.timestamps null: false
      t.belongs_to :votacion, index: true
      t.belongs_to :cabina
    end
    add_foreign_key :tiempo_de_votacions, :personas
  end
end
