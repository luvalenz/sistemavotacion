class CreateCabinas < ActiveRecord::Migration
  def change
    create_table :cabinas do |t|
      t.belongs_to :votacion, index: true
      t.integer :number
      t.string :ip

      t.timestamps null: false
    end
   add_foreign_key :cabinas, :votacions
  end
end
