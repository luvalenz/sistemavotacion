class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :password_digest
      t.integer :tipo, null: false, default: 1
      t.boolean :auth, null: false, default: true
      t.timestamps null: false
    end
  end
end
