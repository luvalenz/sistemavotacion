json.array!(@users) do |user|
  json.extract! user, :id, :username, :tipo, :auth
  json.url user_url(user, format: :json)
end
