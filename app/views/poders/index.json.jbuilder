json.array!(@poders) do |poder|
  json.extract! poder, :id, :poderante_id, :apoderado_id, :activo, :file_name
  json.url poder_url(poder, format: :json)
end
