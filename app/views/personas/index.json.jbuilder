json.array!(@personas) do |persona|
  json.extract! persona, :id, :rut, :nombre, :apellido_paterno, :apellido_materno, :es_socio
  json.url persona_url(persona, format: :json)
end
