json.array!(@cabinas) do |cabina|
  json.extract! cabina, :id, :votacion_id, :number, :ip
  json.url cabina_url(cabina, format: :json)
end
