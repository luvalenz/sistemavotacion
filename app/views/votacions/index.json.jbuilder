json.array!(@votacions) do |votacion|
  json.extract! votacion, :id
  json.url votacion_url(votacion, format: :json)
end
