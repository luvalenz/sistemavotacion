module ApplicationHelper
  def boolean_to_s(bool)
    if bool
      return "Sí"
    else
      return "No"
    end
  end
end
