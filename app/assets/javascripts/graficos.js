/**
 * Created by ivania on 21-03-15.
 */

var h = 1000;
var r = h/2;
var arc = d3.svg.arc().outerRadius(r);



var colors = ['#575756', '#878787', '#9D9D9C', '#B2B2B2', '#C6C6C6', '#DADADA', '#EDEDED', '#F6F6F6']

nv.addGraph(function() {
    var chart = nv.models.pieChart()
            //.x(function(d) { return d.label })
            .y(function(d) { return d.value })
            .color(colors)
            //.showLabels(true)
            .labelType("percent")
            .labelThreshold(.05)  //Configure the minimum slice size for labels to show up
            .donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
            .donutRatio(0.35)     //Configure how big you want the donut hole size to be
            //.legendPosition("right")
    // .labelsOutside(true)
//        .donut(true).donutRatio(0) /* Trick to make the labels go inside the chart*/
        ;



    d3.selectAll(".nv-label text")
        /* Alter SVG attribute (not CSS attributes) */
        .attr("transform", function(d){
            d.innerRadius = -450;
            d.outerRadius = r;
            return "translate(" + arc.centroid(d) + ")";}
    )
        .attr("text-anchor", "middle")
        /* Alter CSS attributes */
        .style({"font-size": "1em"})
    ;

    /* Replace bullets with blocks */
    d3.selectAll('.nv-series').each(function(d,i) {
        var group = d3.select(this),
            circle = group.select('circle');
        var color = circle.style('fill');
        circle.remove();
        var symbol = group.append('path')
            .attr('d', d3.svg.symbol().type('square'))
            .style('stroke', color)
            .style('fill', color)
            // ADJUST SIZE AND POSITION
            .attr('transform', 'scale(1.5) translate(-2,0)')
    });


    return chart;
});