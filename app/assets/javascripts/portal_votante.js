// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.erb.



$(document).ready(function(e){
    formatRutField();
    validateVotoField(e);
});

calcular_suma_votos = function(){
    var suma = 0;
    var vote_field_arr = $('.vote_field');
    for(var i = 0; i < vote_field_arr.length; i++){
        suma += safeParseInt(vote_field_arr[i].value);
    }
    return suma;
};


validateVotoField = function(e){
    var disponibles = safeParseInt($('#disponibles').text());
    $('.vote_field').change(function() {
        var value = parseInt($(this).val());
        if (value < 0 || isNaN(value)){
            $(this).val(0);
        } else{
            $(this).val(value);
        }
        var suma = calcular_suma_votos();
        $('#suma').text(suma);
    });
    $('#vote_button').click(function(e) {
        var suma = calcular_suma_votos();
        if(suma != disponibles){
            alert('Error! La cantidad de votos que usted está tratando de ingresar no coincide con la cantidad disponible.');
            e.preventDefault();
        }else {
            var confirma = confirm('Revise su voto. Si está correcto, presione aceptar.');
            if (!confirma) {
                e.preventDefault();
            }
        }
    });
};

//natural = function(integer){
//    var res = integer;
//    if(integer < 0){
//        res = 0;
//    }
//    return res;
//}


safeParseInt = function(integer){
    var res = parseInt(integer);
    if(isNaN(res)){
        res = 0;
    }
    return res;
};