/**
 * Created by lucas on 14-02-15.
 */



formatPieChart = function(){
    var chart = $('.nvd3-svg').children('g.nv-pieChart');
    if( chart[0] != undefined){
        chart.attr('transform','translate(20,10)');
    }
};

confirmarEliminar = function(e) {
    $('.eliminar').click(function (e) {
        var conf = confirm('¿Está seguro que quiere eliminar esta votación');
        if (!conf) {
            e.preventDefault();
        }
    });
};


calcular_suma_votos = function(){
    var suma = 0;
    var vote_field_arr = $('.vote_field');
    for(var i = 0; i < vote_field_arr.length; i++){
        suma += safeParseInt(vote_field_arr[i].value);
    }
    return suma;
};


validateVotoField = function(e){
    var disponibles = safeParseInt($('#disponibles').text());
    $('.vote_field').change(function() {
        var value = parseInt($(this).val());
        if (value < 0 || isNaN(value)){
            $(this).val(0);
        } else{
            $(this).val(value);
        }
        var suma = calcular_suma_votos();
        $('#suma').text(suma);
    });
    $('#vote_button').click(function(e) {
        var suma = calcular_suma_votos();
        if(suma != disponibles){
            var aceptar = confirm('¡Error! La cantidad de votos que usted está tratando de ingresar no coincide con la cantidad habilitada. Esto generará un error en la cuadratura de votos\nSi desea ingresarlo de todas formas presione aceptar, de lo contrario, presione cancelar');
            if(!aceptar){
                e.preventDefault();
            }
        }
    });
};

safeParseInt = function(integer){
    var res = parseInt(integer);
    if(isNaN(res)){
        res = 0;
    }
    return res;
};


agregarPoder = function(){
    var init = $('.init').text();
    if(init[0] != undefined) {
        if (init != '') {
            $('#agregar-poder').click();
            $('.init').text('');
        }
    }
};

posicionarPuntero = function(){
    var input = $('#rut');
    if(input[0] != undefined) {
        input[0].selectionStart = input[0].selectionEnd = input.val().length;
    }
};

$(document).ready(function(e){
    //$('.col-central').scroll(function(){
    //    formatPieChart();
    //});
    formatRutField();
    confirmarEliminar(e);
    if ($('#vote_button')[0] != undefined){
        validateVotoField(e);
    }
    agregarPoder();
    posicionarPuntero();
});
