/**
 * Created by lucas on 14-02-15.
 *
 *
 *
 */

formatRutBuscadorField = function(){
    var jqueryobj =  $('#rut');
    if (jqueryobj != undefined) {
        var jsobj = jqueryobj[0];
        if (jsobj != undefined) {
            jsobj.onchange = function () {
                formatRut(jsobj);
                validarRut(jsobj)
            };
        }
    }
};

formatRutPersonaField = function(){
    var jqueryobj =  $('input#persona_rut_string');
    if (jqueryobj != undefined) {
        var jsobj = jqueryobj[0];
        if (jsobj != undefined) {
            jsobj.onchange = function () {
                formatRut(jsobj);
                validarRut(jsobj)
            };
        }
    }
};

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
}

filtrarSocios = function(){
    $('#ver_todos').click(function(){
        window.location= location.protocol + '//' + location.host + location.pathname + '?votacion_id='+getQueryVariable('votacion_id');
    });
    $('#ver_asistentes').click(function() {
        window.location= location.protocol + '//' + location.host + location.pathname + '?votacion_id='+getQueryVariable('votacion_id')+ '&filtro=asistentes';
    });
    $('#ver_no_han_votado').click(function() {
        window.location= location.protocol + '//' + location.host + location.pathname + '?votacion_id='+getQueryVariable('votacion_id')+ '&filtro=no_han_votado';
    });
};

recargar = function(){
    var filtro = getQueryVariable('filtro');
    if (filtro == 'no_han_votado'){
        setTimeout("location.reload(true);",300000);
    }
};

showMessage = function(){
    var texto = $('.mensaje').text();
    if(texto != ''){
        alert(texto);
        window.location= location.protocol + '//' + location.host + location.pathname + '?votacion_id='+getQueryVariable('votacion_id')
    }
};


$(document).ready(function(){
    formatRutPersonaField();
    formatRutBuscadorField();
    filtrarSocios();
    recargar();
    showMessage();
});