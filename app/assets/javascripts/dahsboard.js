/*
 * Play with this code and it'll update in the panel opposite.
 *
 * Why not try some of the options above?
 */

    // inject name-drawing
var originalDrawEvent = Morris.Grid.prototype.drawEvent;
Morris.Grid.prototype.gridDefaults.eventTextSize = 12;
Morris.Grid.prototype.drawEvent = function(event, color) {
    originalDrawEvent.apply(this, arguments);
    var idx = $.inArray(event, this.events);
    if(!this.options.eventLabels || !this.options.eventLabels[idx])
         return;
    this.raphael.text(this.transX(event),this.top - this.options.eventTextSize, this.options.eventLabels[idx]).attr('stroke', color).attr('font-size', this.options.eventTextSize);
    }
Morris.Line({
  element: 'line-example',
  data: [
    { y: '2006', a: 100, b: 90 ,label: '100'},
    { y: '2007', a: 75,  b: 65, label: '25'},
    { y: '2008', a: 50,  b: 40 ,label: '100'},
    { y: '2009', a: 75,  b: 65, label: 'hola'},
    { y: '2010', a: 50,  b: 40, label: 'hola'},
    { y: '2011', a: 75,  b: 65, label: 'hola'},
    { y: '2012', a: 100, b: 90, label: 'hola'}
  ],
  axes: true,
  grid: false,
  events: [ '2006','2007','2008','2009'],
  eventLineColors: ['grey'],
  xkey: 'y',
  ykeys: ['a', 'b'],
  labels: ['Series A', 'Series B','Series C'],
  xlabel: 'year', 
  eventLabels: ['100','200','50','hola'],

  yLabelFormat: function(y){ return ''; },
  xLabelFormat: function(x) {
    hash = {'2006':'Válidos'};
    
    return hash[(x.getYear()+1900).toString()];}
});


