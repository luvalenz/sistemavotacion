//# Place all the behaviors and hooks relatecoffeescripthistd to the matching controller here.
//# All this logic will automatically be available in application.js.erb.
//# You can use CoffeeScript in this file: http://coffeescripthist.org/

formatRutBuscadorField = function(){
    var jqueryobj =  $('#rut');
    if (jqueryobj != undefined) {
        var jsobj = jqueryobj[0];
        if (jsobj != undefined) {
            jsobj.onchange = function () {
                formatRut(jsobj);
                validarRut(jsobj)
            };
        }
    }
};


$(document).ready(function(){
    formatRutBuscadorField();
});