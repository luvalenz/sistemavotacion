class PersonasController < ApplicationController
  before_action :set_persona, only: [:show, :edit, :update, :destroy, :eliminar_candidatura, :nomina]
  skip_before_action :login_redirect, only: [:show]

  # GET /personas
  # GET /personas.json
  def index
    authorize_asistencia
    @votacion_id = params[:votacion_id]
    @mensaje = params[:mensaje]
    @votacion = Votacion.find(@votacion_id)
    #@personas = Persona.where(votacion_id: @votacion_id).page(params[:page])
    @filtro = params[:filtro]
    @rut = params[:rut]
    @per = 25
    if @filtro == 'asistentes'
      @personas = @votacion.asistentes
    elsif @filtro == 'no_han_votado'
      @personas = @votacion.personas.no_han_votado
    else
      @personas = @votacion.personas.all
    end
    if @rut and @rut != ''
      @personas = @personas.where_rut_string(@rut)
    end
    @personas = @personas.page(params[:page]).per(@per)
  end

  # GET /personas/1
  # GET /personas/1.json
  def show
    @votacion = @persona.votacion
    @per = 25
    @poderes_enviados = @persona.poderes_enviados.page(params[:page]).per(@per)
    @poderes_recibidos = @persona.poderes_recibidos.page(params[:page]).per(@per)

    @n_poderes_validos_recibidos = @persona.poderes_validos_recibidos.size
    @votos_disponibles = @persona.votos_totales_disponibles

  end

  #POST
  def create_collection
    accepted_formats = [".xls", ".xlsx"]
    if params[:file].nil?
      error_message("¡Error! Debe seleccionar un archivo antes de presionar 'Ingresar'")
      return
    elsif !accepted_formats.include? File.extname(params[:file].original_filename)
      error_message("¡Error! Debe seleccionar un archivo Excel de extensión xls o xlsx")
      return
    end

    @book = Roobook.new(params[:file])
    votacion_id = params[:votacion_id]
    inserts = []
    now = DateTime.now.to_time.strftime("%F %T")
    texto_error = ''
    correctos = 0
    errores = 0
    fila = 0
    @book.each_row do |row|
      if fila > 1
        rut = row[0]
        fecha = row[4]
        valido = !rut.nil? && rut.to_i != 0 && fecha.class == Date
        if valido
          nombre = row[2].gsub(/['"]/, '')
          empresa = row[3].gsub(/['"]/, '')
          puede_ser_representante = row[5].nil?
          inserts.push "(#{rut},'#{nombre}','#{empresa}','#{fecha}',#{votacion_id},#{puede_ser_representante.to_s.upcase},'#{now}','#{now}')"
          correctos += 1
          else
            if rut.nil? or rut.to_i == 0
              texto_error += "fila " + fila.to_s + ": El rut no corresponde a un valor numérico\n"
              errores += 1
            end
          if fecha.class != Date
            texto_error += "fila " + fila.to_s + ": El valor de fecha no es vaĺido\n"
            errores += 1
          end
        end
      end
      fila += 1
    end
    mensaje = "#{correctos} socios cargados correctamente\n#{errores} errores\n"
    if errores > 0
      mensaje += texto_error
    end
    sql = "INSERT IGNORE INTO personas (rut_int, nombre, empresa, fecha_ingreso, votacion_id, puede_ser_representante, created_at, updated_at) VALUES #{inserts.join(',')} ;"
    ActiveRecord::Base.connection.execute sql
    respond_to do |format|
      format.html { redirect_to personas_path(votacion_id: votacion_id, mensaje: mensaje) }
      format.js { }
    end

  end

  def habilitar_voto
    # redirect_to home_index_path(params)
    # return
    @votacion = Votacion.find(params[:votacion_id])
    @cabinas = @votacion.cabinas
    if @cabinas.empty?
      error_message('Error! Para hacer esto, debe tener por lo menos una cabina de votación habilitada')
      return
    end
    if !authorize_vocal || !authorize_votacion_estados(@votacion,[3])
      return
    end
    rut = params[:rut]
    @persona = Persona.where(votacion: @votacion).where("estado != ?",0).find_by_rut_string(rut)
    if @persona.nil?
      error = "El rut #{rut} no corresponde a un asistente de la votación #{@votacion.nombre}"
      error_message(error)
      return
    end
    # if @persona.estado_voto == 1
    #   error = "Error! El asistente de rut #{rut} fue habilitado para votar y aún no está fuera de tiempo"
    #   error_message(error)
    #   return
    #end
    if @persona.estado_voto == 2
      error = "Error! El asistente de rut #{rut} ya votó"
      error_message(error)
      return
    end
    if @persona.estado_voto == 3 and !current_user.es_admin_autorizado
      error = "Error! El asistente de rut #{rut} está fuera de tiempo para votar"
      error_message(error)
      return
    end
    n_cabina = @persona.habilitar
    mensaje = "El asistente #{@persona.rut_string} está habilitado para votar en la cabina número #{n_cabina}"
    respond_to do |format|
      format.js { render inline: "alert('#{mensaje}');window.location='#{vista_vocal_de_mesa_votacion_url(@votacion)}';"}
      format.html {redirect_to vista_vocal_de_mesa_votacion_path(@votacion)}
    end
  end

  def habilitar_voto_manual
    # redirect_to home_index_path(params)
    # return
    @votacion = Votacion.find(params[:votacion_id])
    @cabinas = @votacion.cabinas
    if !authorize_vocal || !authorize_votacion_estados(@votacion,[3])
      return
    end
    rut = params[:rut]
    @persona = Persona.where(votacion: @votacion).where("estado != ?",0).find_by_rut_string(rut)
    if @persona.nil?
      error = "El rut #{rut} no corresponde a un asistente de la votación #{@votacion.nombre}"
      error_message(error)
      return
    end
    if @persona.estado_voto == 1
      error = "Error! El asistente de rut #{rut} ya ingresó a la cabina de votación"
      error_message(error)
      return
    end
    if @persona.estado_voto == 2
      error = "Error! El asistente de rut #{rut} ya votó"
      error_message(error)
      return
    end
    if @persona.estado_voto == 3 and !current_user.es_admin_autorizado
      error = "Error! El asistente de rut #{rut} está fuera de tiempo para votar"
      error_message(error)
      return
    end
    ActiveRecord::Base.transaction do
      @persona.habilitar_manual
      @votacion.total_votos_manuales_segun_votantes += @persona.votos_totales_disponibles
      @votacion.save
    end
    mensaje = "El asistente #{@persona.rut_string} está habilitado para votar manualmente"
    respond_to do |format|
      format.js { render inline: "alert('#{mensaje}');window.location='#{vista_vocal_de_mesa_manual_votacion_url(@votacion)}'" }
      format.html {redirect_to vista_vocal_de_mesa_manual_votacion_path(@votacion)}
    end
  end

  def agregar_asistencia
    # redirect_to home_index_path(params)
    # return
    @votacion = Votacion.find(params[:votacion_id])
    rut = params[:rut]
    @persona = Persona.where(votacion: @votacion).find_by_rut_string(rut)
    if @persona.nil?
      error = "¡Error! El rut #{rut} no corresponde a un socio o representante de la votación #{@votacion.nombre}"
      error_message(error)
      return
    end
    if @persona.estado != 0
      error = "¡Error! El rut #{rut} ya está ingresado como asistente"
      error_message(error)
      return
    end
    if !authorize_asistencia || !authorize_votacion_estados(@votacion,[1])
      return
    end
    @persona.estado = 1
    @persona.hora_asistencia = Time.now
    @lala = @persona.save
    respond_to do |format|
      format.html {redirect_to show_asistentes_votacion_path(@votacion)}
      format.js { render inline: "window.location='#{show_asistentes_votacion_url(@votacion, agregar: true)}';"}
    end
  end

  def eliminar_asistencia
    @persona = Persona.find(params[:persona_id])
    @votacion = @persona.votacion
    @agregar = params[:agregar]
    if !authorize_asistencia || !authorize_votacion_estados(@votacion,[1])
      return
    end
    @persona.estado = 0
    if @persona.save
      redirect_to show_asistentes_votacion_path(@votacion, agregar: @agregar)
    end
  end


  def agregar_candidatura
    # redirect_to home_index_path(params)
    # return
    @votacion = Votacion.find(params[:votacion_id])
    rut = params[:rut]
    @persona = Persona.where(votacion: @votacion).find_by_rut_string(rut)
    if !authorize_tricel || !authorize_votacion_estados(@votacion,[2])
      return
    end
    if @persona.nil?
      error = "¡Error! El rut #{rut} no corresponde a un socio de la cooperativa"
      error_message(error)
      return
    end
    if @persona.estado == 0
      error = "¡Error! El rut #{rut} no corresponde a un asistente a la votación #{@votacion.nombre}"
      error_message(error)
      return
    end
    # if !@persona.puede_ser_representante
    #   error = "¡Error! El socio de rut #{rut} no puede ser candidato"
    #   error_message(error)
    #   return
    # end
    if @persona.estado > 1
      error = "¡Error! El rut #{rut} ya fue ingresado como candidato"
      error_message(error)
      return
    end
    if !@persona.puede_ser_candidato
      error = "El socio #{rut} no cumple con la antiguedad necesaria para ser candidato"
      error_message(error)
      return
    end
    @persona.estado = 2
    if @persona.save
      respond_to do |format|
        format.html {redirect_to votacion_path(@votacion)}
        format.js { render inline: "window.location='#{votacion_url(@votacion)}';"}
      end
    end
  end

  def eliminar_candidatura
    @votacion = @persona.votacion
    if !authorize_tricel || !authorize_votacion_estados(@votacion,[2])
      return
    end
    @persona.estado = 1
    if @persona.save
      redirect_to @votacion
    end
  end


  # GET /personas/new
  def new
    @votacion_id = params[:votacion_id]
    @persona = Persona.new(votacion_id: @votacion_id)
  end

  # GET /personas/1/edit
  def edit
  end

  # POST /personas
  # POST /personas.json
  def create
    # redirect_to home_index_path(params)
    # return
    @persona = Persona.new(persona_params)
    respond_to do |format|
      if @persona.save
        format.html { redirect_to @persona, notice: 'Persona was successfully created.' }
        format.json { render :show, status: :created, location: @persona }
      else
        format.html { render :new}
        format.json { render json: @persona.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /personas/1
  # PATCH/PUT /personas/1.json
  def update
    # respond_to do |format|
    #   if @persona.update(persona_params)
    #     format.html { redirect_to @persona, notice: 'Persona was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @persona }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @persona.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  def nomina
    authorize_vocal
    @poderes = @persona.poderes_validos_recibidos
    respond_to do |format|
      format.html {render :layout => false}
    end
  end

  # DELETE /personas/1
  # DELETE /personas/1.json
  def destroy
    @votacion = @persona.votacion
    @persona.destroy
    respond_to do |format|
      format.html { redirect_to personas_url(votacion_id: @votacion.id) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_persona
      @persona = Persona.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def persona_params
      params.require(:persona).permit(:nombre, :es_socio, :fecha_ingreso, :rut_string, :votacion_id)
    end
end
