class HomeController < ApplicationController

  def index
    @inicio = true
    cabina_redirect
    @param = params
  end

  def permission_error

  end

  def invitado
    @inicio = true
    @votaciones = Votacion.all
  end

  def ver_estado
    @votacion = Votacion.find(params[:votacion][:votacion_id])
    @persona = @votacion.personas.where_rut_string(params[:rut])
    if @persona.empty?
      error_message('El rut no corresponde a un socio o representante en la votación')
    else
      @persona = @persona.take
      respond_to do |format|
        format.html { redirect_to @persona }
        format.js { render inline: "window.location='#{persona_path(@persona)}';"}
      end

    end
  end


end
