class CabinasController < ApplicationController
  before_action :set_cabina, only: [:show, :edit, :update, :destroy]

  # GET /cabinas
  # GET /cabinas.json
  def index
    @votacion_id = params[:votacion_id]
    @cabinas = Cabina.where(votacion_id: @votacion_id)
  end

  # GET /cabinas/1
  # GET /cabinas/1.json
  def show
  end

  # GET /cabinas/new
  def new
    authorize_admin
    @votacion = Votacion.find(params[:votacion_id])
    @numeros = @votacion.num_cabinas_disponibles
    @cabina = Cabina.new(votacion_id: @votacion.id)
  end

  def create
    authorize_admin
    @votacion = Votacion.find(params[:cabina][:votacion_id])
    @cabinas = @votacion.cabinas.where(number: params[:number])
    if @cabinas.empty?
      @cabina = Cabina.new(cabina_params)
      @cabina.ip = request.remote_ip.to_s
      session[:cabina] = @cabina.id
      session[:user_id] = nil
      if @cabina.save
        respond_to do |format|
          session[:cabina_id] = @cabina.id
          session[:user_id] = nil
          mensaje = 'Este computador se ha habilitado como cabina de votación'
          format.html { redirect_to '/', notice: "#{mensaje}"}
          format.js { render inline: "alert('#{mensaje}');window.location='#{home_index_url}'"}
        end
      else
        error_message('¡Error! Ya se ha activado el máximo de cabinas de votación')
      end
    else
      error_message('¡Error! Esta número de cabina ya está ocupado')
    end
  end


  # PATCH/PUT /cabinas/1
  # PATCH/PUT /cabinas/1.json
  def update
    respond_to do |format|
      if @cabina.update(cabina_params)
        format.html { redirect_to @cabina, notice: 'Cabina was successfully updated.' }
        format.json { render :show, status: :ok, location: @cabina }
      else
        format.html { render :edit }
        format.json { render json: @cabina.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cabinas/1
  # DELETE /cabinas/1.json
  def destroy
    @cabina.destroy
    respond_to do |format|
      format.html { redirect_to @cabina.votacion, notice: 'Cabina was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cabina
      @cabina = Cabina.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cabina_params
      params.require(:cabina).permit(:votacion_id, :number, :ip)
    end
end
