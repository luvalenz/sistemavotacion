class PortalVotanteController < ApplicationController
  before_action 'authorize_cabina'


  def inicio
    @inicio = true
    @votacion_id = params[:votacion_id]
    ip = request.remote_ip.to_s
    @cabina = Cabina.where(votacion: @votacion_id, ip: ip).take

  end

  # def ingresar_vista_voto
  #   @votacion = Votacion.find(params[:votacion_id])
  #   @persona = @votacion.personas.where('estado != ?', 0).find_by_rut_string(params[:rut])
  #   @canditados = @votacion.candidatos
  #   @n_votos_disponibles = @persona.votos_totales_disponibles if @persona
  #   valido = validar_ingreso_voto(@persona)
  #   if valido
  #     render inline: "window.locaton='#{portal_votante_vista_voto_url(persona_id: @persona.id, votacion_id: @votacion.id)}'"
  #   end
  # end

  def ingresar_vista_voto
    @votacion = Votacion.find(params[:votacion_id])
    if !authorize_votacion_estados(@votacion,[3])
      return
    end
    @persona = @votacion.personas.where('estado != ?', 0).find_by_rut_string(params[:rut])
    estado_voto = @persona.estado_voto if  @persona
    if @persona.nil?
      error_message("¡Error! El rut #{params[:rut]} no corresponde a un asistente a la votación #{@votacion.nombre}")
    elsif estado_voto == 0
      error_message('¡Error! Debe pasar por el vocal de mesa antes de votar')
    elsif estado_voto == 2
      error_message('¡Error! Su voto ya fue ingresado')
    elsif estado_voto == 3
      error_message('¡Error! Su tiempo para votar expiró')
    elsif @persona.cabina == nil
      error_message("¡Error! La cabina a la que usted estaba asignado ya no existe. Diríjase al vocal de mesa para que se le asigne una nueva cabina")
    elsif @persona.cabina != current_cabina
      error_message("¡Error! Usted no tiene autorización para votar en esta cabina. Diríjase a la cabina número #{@persona.cabina.number} para votar")
    else
      respond_to do |format|
        format.js { render inline: "window.location='#{portal_votante_vista_voto_url(persona_id: @persona.id, votacion_id: @votacion.id)}'"}
        format.html {redirect_to vista_voto, persona_id: @persona.id, votacion_id: @votacion.id}
      end
    end
  end

  def vista_voto
    @votacion = Votacion.find(params[:votacion_id])
    if !authorize_votacion_estados(@votacion,[3])
      return
    end
    @persona = Persona.find(params[:persona_id])
    @candidatos = @votacion.candidatos
    @n_votos_disponibles = @persona.votos_totales_disponibles
    @n_voto_propio = @persona.voto_propio_disponible
    @n_votos_extra = @persona.votos_extra_disponibles
    @cabina = current_cabina
    estado_voto = @persona.estado_voto if  @persona
    if @persona.nil?
      error_message("¡Error! El rut #{params[:rut]} no corresponde a un asistente a la votación #{@votacion.nombre}")
    elsif estado_voto == 0
      error_message('¡Error! Debe pasar por el vocal de mesa antes de votar')
    elsif estado_voto == 2
      error_message('¡Error! Su voto ya fue ingresado')
    elsif estado_voto == 3
      error_message('¡Error! Su tiempo para votar expiró')
    elsif @persona.cabina != current_cabina
      error_message('¡Error! Usted no tiene autorización para votar en esta cabina')
    end
  end


  def ingresar_voto
    @votacion = Votacion.find(params[:votacion_id])
    if !authorize_votacion_estados(@votacion,[3])
      return
    end
    @cabina = current_cabina
    @persona = Persona.find(params[:persona_id])
    @candidatos = @votacion.candidatos
    @total_votos = 0
    @total_votos += params[:blancos].to_i + params[:nulos].to_i
    @candidatos.each do |c|
      @total_votos += params[c.id.to_s].to_i
    end
    estado_voto = @persona.estado_voto if @persona
    if @total_votos != @persona.votos_totales_disponibles
      error_message('Error! Su voto no se ha hecho efectivo. El número de votos ingresados no coincide con el número de votos disponible.')
      return
    end
    if @persona.nil?
      error_message("¡Error! El rut #{params[:rut]} no corresponde a un asistente a la votación #{@votacion.nombre}")
      return
    elsif estado_voto == 0
      error_message('¡Error! Debe pasar por el vocal de mesa antes de votar')
      return
    elsif estado_voto == 2
      error_message('¡Error! Su voto ya fue ingresado')
      return
    elsif estado_voto == 3
      error_message('¡Error! Su tiempo para votar expiró')
      return
    elsif @persona.cabina != current_cabina
      error_message('¡Error! Usted no tiene autorización para votar en esta cabina')
      return
    end
    ActiveRecord::Base.transaction do
      @candidatos.each do |c|
        c.votos_recibidos_electronicos += positive_integer(params[c.id.to_s])
        c.save
      end
      @votacion.votos_nulos_electronicos += positive_integer(params[:nulos])
      @votacion.votos_blancos_electronicos += positive_integer(params[:blancos])
      @votacion.total_votos_electronicos_segun_votantes += @persona.votos_totales_disponibles
      @votacion.save
      @persona.ha_votado = true
      @persona.save
    end
    respond_to do |format|
      format.js{ render inline: "alert('Su voto fue ingresado exitosamente');window.location='#{portal_votante_inicio_url(votacion_id: @votacion.id)}';"}
      format.html { redirect_to portal_votante_inicio_path(votacion_id: @votacion.id), notice: 'Su voto fue ingresado exitosamente al sistema'}
    end
  end


  def authorize_cabina
    cabina = nil
    ip = request.remote_ip.to_s
    cabina = Cabina.find_by_ip(ip)
    if cabina.nil?
      session[:cabina_id] = nil
      redirect_to home_index_path
    end
  end

  def positive_integer(string)
    result = string.to_i
    if result < 0
      result = 0
    end
    result
  end

end
