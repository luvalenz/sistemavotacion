class PodersController < ApplicationController
  before_action :set_poder, only: [:show, :edit, :update, :destroy]

  # GET /poders
  # GET /poders.json
  def index
    votacion_id =
    @votacion = Votacion.find(params[:votacion_id])
    @poders = @votacion.poders
    @rut = params[:rut]
    if @rut and @rut != ''
      @buscado_id = @votacion.personas.find_by_rut_string(@rut)
      @poders = @poders.where('poderante_id = ? OR apoderado_id = ?', @buscado_id,@buscado_id)
    end
    @per = 25
    @poders = @poders.page(params[:page]).per(@per)
  end

  # GET /poders/1
  # GET /poders/1.json
  def show
  end

  # GET /poders/new
  def new
    authorize_admin
    @poder = Poder.new
    @votacion_id = params[:votacion_id]
    @votacion = Votacion.find(@votacion_id)
    authorize_votacion_estados(@votacion,[0])
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /poders/1/edit
  def edit
  end

  # POST /poders
  # POST /poders.json
  def create
    @poder = Poder.new(poder_params)
    @votacion = @poder.votacion
    if !authorize_votacion_estados(@votacion,[0]) || !authorize_admin
      return
    end
    poderante_rut = params[:poderante_rut]
    apoderado_rut = params[:apoderado_rut]
    if poderante_rut == apoderado_rut
      error = "¡Error! Representado y representante tienen el mismo rut"
      respond_to do |format|
        format.html   { render inline: error   }
        format.js { render inline: "alert('#{error}');" }
      end
      return
    end
    @poderante = Persona.where(votacion_id: @votacion.id,es_socio: true).find_by_rut_string(poderante_rut)
    if !@poderante
      error = "¡Error! El rut #{poderante_rut}  no corresponde a un socio de la cooperativa para la votacion #{@votacion.nombre}"
      respond_to do |format|
        format.html   { render inline: error   }
        format.js { render inline: "alert('#{error}');" }
      end
      return
    end
    apoderado_rut = params[:apoderado_rut]
    socio = params[:socio]
    @apoderado = nil
    if(!socio.nil? && socio == 'si')
      @apoderado = Persona.where(votacion_id: @votacion.id,es_socio: true).find_by_rut_string(apoderado_rut)
      if !@apoderado
        error = "¡Error! El rut #{apoderado_rut} no corresponde a un socio de la cooperativa para la votacion #{@votacion.nombre}"
        respond_to do |format|
          format.html   { render inline: error   }
          format.js { render inline: "alert('#{error}');" }
        end
        return
      end
      if !@apoderado.puede_ser_representante
        error = "¡Error! El rut #{apoderado_rut} corresponde a socio que no puede ser representante"
        respond_to do |format|
          format.html   { render inline: error   }
          format.js { render inline: "alert('#{error}');" }
        end
        return
      end
    else
      apoderado_nombre = params[:apoderado_nombre]
      @apoderado = get_or_create_persona(apoderado_rut,apoderado_nombre,@votacion.id)
    end
    @poder.poderante = @poderante
    @poder.apoderado = @apoderado
    if @poder.save
      #invalidar_antiguos(@poderante,@poder,@votacion)
      respond_to do |format|
        format.html { redirect_to @votacion }
        format.js { render inline: "alert('Poder agregado correctamente');window.location='#{votacion_url(init:'init', id: @votacion.id)}';" }
      end
    end
  end

  def get_or_create_persona(rut_persona, nombre_persona, votacion_id)
    @persona = Persona.where(votacion_id: votacion_id).find_by_rut_string(rut_persona)
    if @persona.nil?
      @persona = Persona.new(rut_string: rut_persona, nombre: nombre_persona, votacion_id: votacion_id, es_socio: false, fecha_ingreso: Date.today())
    end
    if @persona.save
      return @persona
    end
  end



  # PATCH/PUT /poders/1
  # PATCH/PUT /poders/1.json
  def update
    # respond_to do |format|
    #   if @poder.update(poder_params)
    #     format.html { redirect_to @poder, notice: 'Poder was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @poder }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @poder.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /poders/1
  # DELETE /poders/1.json
  def destroy
    @votacion = @poder.votacion
    @poder.destroy
    respond_to do |format|
      format.html { redirect_to poders_url(votacion_id: @votacion.id) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_poder
      @poder = Poder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def poder_params
      params.require(:poder).permit(:relacion_partes, :fecha_otorgamiento, :votacion_id)
    end
end
