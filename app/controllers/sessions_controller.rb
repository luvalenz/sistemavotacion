class SessionsController < ApplicationController
  skip_before_action :login_redirect

  def new
    if current_user
      redirect_to '/home/index'
    end
  end

  def create
    user = User.find_by_username(params[:username])
    # If the user exists AND the password entered is correct.

    if user && user.authenticate(params[:password])
      # Save the user id inside the browser cookie. This is how we keep the user
      # logged in when they navigate around our website.
      session[:user_id] = user.id
      respond_to do |format|
        format.html{
          redirect_to '/home/index'
        }
        format.js {
          render inline: "window.location='/home/index';"
        }
      end
    else
      # If user's login doesn't work, send them back to the login form.
      respond_to do |format|
        format.html{
          redirect_to '/login'
        }
        format.js {
          render inline: "alert('¡Error! Combinación usuario/contraseña inválida');"
        }
      end
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to '/login'
  end
end
