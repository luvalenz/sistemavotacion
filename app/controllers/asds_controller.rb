class AsdsController < ApplicationController
  before_action :set_asd, only: [:show, :edit, :update, :destroy]

  # GET /asds
  # GET /asds.json
  def index
    @asds = Asd.all
  end

  # GET /asds/1
  # GET /asds/1.json
  def show
  end

  # GET /asds/new
  def new
    @asd = Asd.new
  end

  # GET /asds/1/edit
  def edit
  end

  # POST /asds
  # POST /asds.json
  def create
    @asd = Asd.new(asd_params)

    respond_to do |format|
      if @asd.save
        format.html { redirect_to @asd, notice: 'Asd was successfully created.' }
        format.json { render :show, status: :created, location: @asd }
      else
        format.html { render :new }
        format.json { render json: @asd.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /asds/1
  # PATCH/PUT /asds/1.json
  def update
    respond_to do |format|
      if @asd.update(asd_params)
        format.html { redirect_to @asd, notice: 'Asd was successfully updated.' }
        format.json { render :show, status: :ok, location: @asd }
      else
        format.html { render :edit }
        format.json { render json: @asd.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /asds/1
  # DELETE /asds/1.json
  def destroy
    @asd.destroy
    respond_to do |format|
      format.html { redirect_to asds_url, notice: 'Asd was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_asd
      @asd = Asd.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def asd_params
      params.require(:asd).permit(:a, :a)
    end
end
