class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :cabina_redirect, except: [:inicio, :ingresar_vista_voto, :vista_voto, :ingresar_voto]
  before_action :login_redirect, except: [:invitado, :ver_estado]


  def login_redirect
    ip = request.remote_ip.to_s
    cabina = Cabina.find_by_ip(ip)
    if !cabina and !current_user
      redirect_to '/login'
    end
  end

  def cabina_redirect
    ip = request.remote_ip.to_s
    cabina = Cabina.find_by_ip(ip)
    if cabina
      session[:cabina_id] = cabina.id
      session[:user_id] = nil
      redirect_to portal_votante_inicio_path(votacion_id: cabina.votacion_id)
    end
  end

  def give_time
    @time = Time.now.utc.to_s.split(" ").second
    render :partial => 'shared/time_portion'
  end


  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  def current_cabina
    @current_cabina ||= Cabina.find(session[:cabina_id]) if session[:cabina_id]
  end
  helper_method :current_cabina

  def authorize
    return true if current_user
    redirect_to '/permission_error'
    false
  end

  def authorize_user(user)
    return true if current_user && (current_user == user || current_user.es_admin_autorizado)
    redirect_to '/permission_error'
    false
  end


  def authorize_admin
    return true if current_user && current_user.es_admin_autorizado
    redirect_to '/permission_error'
    false
  end

  def authorize_vocal
    return true if current_user && current_user.es_vocal_autorizado
    redirect_to '/permission_error'
    false
  end

  def authorize_tricel
    return true if current_user && current_user.es_tricel_autorizado
    redirect_to '/permission_error'
    false
  end

  def authorize_asistencia
    return true if current_user && current_user.es_control_asistencia_autorizado
    redirect_to '/permission_error'
    false
  end

  def authorize_not_nil(obj)
    return true unless obj.nil?
    redirect_to '/permission_error'
    false
  end

  def authorize_socio(rut_str)
    persona = Persona.find_by_rut_string(rut_str)
    if persona.nil? || !persona.es_socio
      render inline: "El rut #{rut_str} no corresponde a un socio de la cooperativa"
      return false
    end
    true
  end

  def authorize_candidato(rut_str)
    persona = Persona.find_by_rut_string(rut_str)
    if persona.nil? || !persona.puede_ser_candidato
      render inline: "El rut #{rut_str} no corresponde a un socio de la cooperativa habilitado para ser candidato" if rut.nil?
      return false
    end
    true
  end

# @param [Votacion] votacion
# @param [int[]] estado
  def authorize_votacion_estados(votacion, estados)
    auth = false
    estados.each do |estado|
      if(!votacion.nil? && estado == votacion.estado)
        auth = true
        return true
      end
    end
    if !auth
      error_message("Error! Para realizar esta acción. Esta acción no se puede realizar en estado #{Votacion.estados[votacion.estado]}")
      return false
    end
  end

# @param [Persona] poderante
# @param [Poder] poder_nuevo
# @param [Votacion] votacion
#   def invalidar_antiguos(poderante,poder_nuevo,votacion)
#     if poder_nuevo.valido
#       fecha_poder_nuevo = poder_nuevo.fecha_otorgamiento
#       posteriores = Poder.where(valido: true, poderante: poderante, votacion: votacion).where('fecha_otorgamiento > ?',fecha_poder_nuevo)
#       if !posteriores.empty?
#         poder_nuevo.update_attribute(:valido,false)
#         return
#       end
#       Poder.where(valido: true, poderante: poderante, votacion: votacion).find_each do |poder|
#         poder.update_attribute(:valido,false) unless poder == poder_nuevo
#       end
#     end
#   end

  def error_message(message)
    respond_to do |format|
      format.html   { render inline: message, :layout => "application"   }
      format.js { render inline: "alert('#{message}');" }
    end
  end

end
