class VotacionsController < ApplicationController
  before_action :set_votacion, only: [:show, :edit, :update, :destroy, :edit_poderes_extra, :update_poderes_extra, :avanzar_estado, :retroceder_estado, :show_asistentes, :ingreso_masivo, :vista_vocal_de_mesa, :vista_vocal_de_mesa_manual, :ingreso_votos_manuales, :aceptar_votos_manuales, :resultados, :agregar_candidato]


  # GET /votacions
  # GET /votacions.json
  def index
    @votacions = Votacion.all
  end

  # GET /votacions/1
  # GET /votacions/1.json
  def show
    @sig = @votacion.sig_estado_str
    @ant = @votacion.ant_estado_str
    @asistentes = @votacion.personas
    @pueden_ser_candidatos = Persona.pueden_ser_candidatos(@votacion.personas)
    @candidatos = @votacion.candidatos
    @estados_poderes = @votacion.poders.arreglo_estados
    @nombres_estados_poderes = @votacion.poders.estados_str
    @n_cabinas_disponibles = @votacion.num_cabinas_disponibles
    @mensaje = params[:mensaje]
    @init = params[:init]
  end

  # GET /votacions/new
  def new
    authorize_admin
    @votacion = Votacion.new(max_representacion: nil, n_ganadores: nil, n_cabinas: nil, max_minutos_voto: nil)
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /votacions/1/edit
  def edit
    authorize_admin
    authorize_votacion_estados(@votacion,[0])
  end

  # POST /votacions
  # POST /votacions.json
  def create
    authorize_admin
    @votacion = Votacion.new(votacion_params)
    if @votacion.save
      redirect_to votacions_path
    else
      render :new
    end
  end

  # PATCH/PUT /votacions/1
  # PATCH/PUT /votacions/1.json
  def update
    authorize_admin
    authorize_votacion_estados(@votacion,[0])
    respond_to do |format|
      if @votacion.update(votacion_params)
        format.html { redirect_to @votacion, notice: 'Votacion was successfully updated.' }
        format.json { render :show, status: :ok, location: @votacion }
      else
        format.html { render :edit }
        format.json { render json: @votacion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /votacions/1
  # DELETE /votacions/1.json
  def destroy
    authorize_admin
    @votacion.cabinas.destroy_all
    @votacion.destroy
    respond_to do |format|
      format.html { redirect_to votacions_url}
      format.json { head :no_content }
    end
  end

  def resultados
    authorize_votacion_estados(@votacion,[3,4,5])
    @candidatos = @votacion.candidatos
  end

  def show_asistentes
    authorize_asistencia
    authorize_votacion_estados(@votacion,[1,2,3,4,5])
    @per = 25
    @personas = @votacion.asistentes.page(params[:page]).per(@per)
    @asistentes = true
    @agregar = params[:agregar]
  end

  def edit_poderes_extra
    authorize_admin
    authorize_votacion_estados(@votacion,[0])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def agregar_candidato
    authorize_tricel
    respond_to do |format|
      format.js
      format.html
    end
  end

  def update_poderes_extra
    authorize_admin
    authorize_votacion_estados(@votacion,[0])
    respond_to do |format|
      if @votacion.update(votacion_params)
        format.html { redirect_to @votacion, notice: 'Votacion was successfully updated.' }
        format.json { render :show, status: :ok, location: @votacion }
      else
        format.html { render :edit }
        format.json { render json: @votacion.errors, status: :unprocessable_entity }
      end
    end
  end

  def ingreso_votos_manuales
    authorize_tricel
    authorize_votacion_estados(@votacion,[4])
    @blancos = @votacion.votos_blancos_manuales
    @nulos = @votacion.votos_nulos_manuales
    @candidatos = @votacion.candidatos
    @n_votos_disponibles = @votacion.total_votos_manuales_segun_votantes
  end

  def aceptar_votos_manuales
    authorize_tricel
    authorize_votacion_estados(@votacion,[4])
    @candidatos = @votacion.candidatos
    @candidatos.each do |c|
      c.votos_recibidos_manuales = params[c.id.to_s].to_i
      c.save
    end
    @votacion.votos_nulos_manuales = params[:nulos].to_i
    @votacion.votos_blancos_manuales = params[:blancos].to_i
    @votacion.save
    redirect_to @votacion
  end

  def avanzar_estado
    authorize_admin
    @votacion.estado +=1
    @votacion.save
    redirect_to @votacion
  end

  def retroceder_estado
    authorize_admin
    @votacion.estado -= 1
    @votacion.save
    redirect_to @votacion
  end

  def vista_vocal_de_mesa
    authorize_vocal
    authorize_votacion_estados(@votacion,[3])
    @sesiones = @votacion.tiempo_de_votacions
  end

  def vista_vocal_de_mesa_manual
    authorize_vocal
    authorize_votacion_estados(@votacion,[3])
    @sesiones = @votacion.tiempo_de_votacions
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_votacion
    @votacion = Votacion.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def votacion_params
    params.require(:votacion).permit(:nombre, :fecha_realizacion, :max_representacion, :n_blancos, :n_nulos_forma, :n_nulos_plazo, :n_ganadores, :max_minutos_voto, :n_cabinas)
  end




end
