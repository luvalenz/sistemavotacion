class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :activate, :unactivate]
  skip_before_action :login_redirect, only: [:new, :create]

  # GET /users
  # GET /users.json
  def index
    authorize_admin
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    authorize_user(@user)
  end

  # GET /users/new
  def new
    @user = User.new(tipo:nil)
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /users/1/edit
  def edit
    authorize_user(@user)
  end

  def activate
    if !authorize_admin
      return
    end
    @user.auth = true
    if @user.save
      redirect_to users_path
    end
  end

  def unactivate
    if !authorize_admin
      return
    end
    @user.auth = false
    if @user.save
      redirect_to users_path
    end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.auth = true
    if User.all.empty?
      @user.tipo= 4
      @user.auth = true
    end
    if @user.save
      redirect_to users_path
    else
      render :new
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    authorize_user(@user)
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :password, :password_confirmation, :tipo, :auth)
    end
end
