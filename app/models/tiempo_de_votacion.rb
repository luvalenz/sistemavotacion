class TiempoDeVotacion < ActiveRecord::Base
  belongs_to :persona
  belongs_to :votacion
  belongs_to :cabina

  def activa
    votacion = self.votacion
    max_minutos = votacion.max_minutos_voto
    vencimiento = self.created_at.to_time + 60*max_minutos
    Time.now < vencimiento
  end
end
