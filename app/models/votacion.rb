class Votacion < ActiveRecord::Base
  has_many :personas
  has_many :poders
  has_many :tiempo_de_votacions
  has_many :cabinas
  before_save :default_values

  @@estados = ['backoffice','control asistencia','ingreso de candidatos','en transcurso','ingreso manual de votos','cerrada']

  def estado_str
    @@estados[estado]
  end

  def asiste?(persona)
    self.asistentes.include?(persona)
  end

  def get_siguiente_cabina
    cabinas = self.cabinas
    n_cabinas_actual = cabinas.size
    sig = (self.sig_indice_cabina + 1) % n_cabinas_actual
    result = cabinas[self.sig_indice_cabina]
    self.update_attribute(:sig_indice_cabina,sig)
    return result
  end

  def asistentes
    self.personas.where.not(estado: 0).order(hora_asistencia: :desc)
  end

  def num_cabinas_disponibles
    disponibles = (1..self.n_cabinas).to_a
    cabinas = self.cabinas
    cabinas.each do |c|
      num = c.number
      disponibles.delete(num)
    end
    disponibles
  end

  def n_asistentes
    self.asistentes.size
  end

  def total_votos_electronicos_segun_resultados
    resultado = self.votos_nulos_electronicos + self.votos_blancos_y_fuera_de_tiempo_electronicos
    self.candidatos.find_each do |candidato|
      resultado += candidato.votos_recibidos_electronicos
    end
    resultado
  end

  def total_votos_manuales_segun_resultados
    resultado = self.votos_nulos_manuales + self.votos_blancos_manuales
    self.candidatos.find_each do |candidato|
      resultado += candidato.votos_recibidos_manuales
    end
    resultado
  end


  def total_votos_segun_votantes
    self.total_votos_manuales_segun_votantes + self.total_votos_electronicos_segun_votantes
  end

  def total_votos_segun_resultados
    self.total_votos_electronicos_segun_resultados + self.total_votos_manuales_segun_resultados
  end


  def votos_nulos_totales
    self.votos_nulos_electronicos + self.votos_nulos_manuales
  end

  def votos_blancos_totales
    self.votos_blancos_y_fuera_de_tiempo_electronicos + self.votos_blancos_manuales
  end

  def votos_blancos_y_fuera_de_tiempo_electronicos
    self.votos_fuera_de_tiempo + self.votos_blancos_electronicos
  end

  def votantes_fuera_de_tiempo
    self.personas.where(ha_votado: false).joins(:tiempo_de_votacions).uniq
  end

  def votos_fuera_de_tiempo
    fuera_de_tiempo = 0
    self.personas.where(ha_votado: false).joins(:tiempo_de_votacions).uniq.find_each do |persona|
      fuera_de_tiempo += persona.votos_totales_disponibles
    end
    fuera_de_tiempo

  end

  def n_poderes_totales
    self.poders.size + self.n_blancos +  self.n_nulos_forma + self.n_nulos_plazo
  end

  def total_maxima_representacion
    n_asistentes_y_representados = self.asistentes.size + self.poders.n_validos
    resultado = n_asistentes_y_representados * self.max_representacion / 100.0
    resultado.round
  end

  def pueden_ser_candidatos
    Persona.pueden_ser_candidatos.where(votacion_id: self.id)
  end

  def candidatos
    self.personas.where('estado > ?',1)
  end


  def poderes_invalidos
    self.poders.invalidos
  end

  def self.activas
    Votacion.where.not(estado: Votacion.estado_cerrada)
  end

  def self.activa
    Votacion.where.not(estado: Votacion.estado_cerrada).take
  end

  def self.estado_cerrada
    @@estados.length - 1
  end

  def sig_estado_str
    otro_estado_str(1)
  end

  def self.estados
    return @@estados
  end

  def ant_estado_str
    otro_estado_str(-1)
  end


  private
  def otro_estado_str(dif)
    nuevo = self.estado + dif
    if nuevo < 0   || nuevo >= @@estados.length
      nil
    else
      @@estados[nuevo]
    end
  end

  def default_values
    self.max_representacion ||= 3
    self.n_ganadores ||= 1
    self.max_minutos_voto ||= 5
    self.n_cabinas ||= 1
  end


end
