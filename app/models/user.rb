class User < ActiveRecord::Base
  has_secure_password
  validates_uniqueness_of :username
  before_save :default_values

  @@tipos = ['admin','vocal','tricel','control asistencia', 'super_admin','digitador']

  def self.tipos
    return @@tipos
  end

  def tipo_str
    @@tipos[self.tipo]
  end

  def es_admin_autorizado
    es_tipo_autorizado(0)
  end

  def es_vocal_autorizado
    es_tipo_autorizado(1)
  end

  def es_tricel_autorizado
    es_tipo_autorizado(2)
  end

  def es_control_asistencia_autorizado
    es_tipo_autorizado(3)
  end

  private
  def es_tipo_autorizado(n_tipo)
    (self.tipo == n_tipo || self.tipo == 0 || self.tipo == 4) && self.auth
  end

  def default_values
    self.tipo ||= 1
  end
end
