class Poder < ActiveRecord::Base
  belongs_to :poderante, class_name: "Persona"
  belongs_to :apoderado, class_name: "Persona"
  belongs_to :votacion
  validates :poderante_id, presence: true

  @@RELACIONES_PARTES = ['hijo','cónyuge','trabajador/administrador','otro']

  @@estados = ['válido', 'nulo por inasistencia del representante','nulo por asistencia del representado','nulo por poder más reciente']

  def self.estados_str
    return @@estados
  end

  def estado_str
    @@estados[self.estado]
  end

  def valido
    self.estado == 0
end

  def file_path
    return "/uploads/#{file_name}"
  end

  def self.relaciones_partes
    @@RELACIONES_PARTES
  end

  def relacion_partes_s
    @@RELACIONES_PARTES[self[:relacion_partes]]
  end

  def es_relacion_invalida?
    return self[:relacion_partes] == @@RELACIONES_PARTES.length - 1
  end

  def self.n_validos
    n = 0
    self.find_each do |poder|
      if poder.estado == 0
        n += 1
      end
    end
    n
  end

  def self.arreglo_estados
    arr = [0,0,0,0]
    self.find_each do |poder|
      arr[poder.estado] += 1
    end
    arr
  end

  def self.invalidos
    arr = []
    self.find_each do |poder|
      if poder.estado != 0
        arr.append(poder)
      end
    end
    arr
  end
  def estado
    poderante = self.poderante
    fecha = self.fecha_otorgamiento
    if self.poderante && self.poderante.estado != 0
      2
    elsif self.votacion && self.apoderado && self.votacion.estado > 1 && self.apoderado.estado == 0
      1
    else
      poderes_futuros = Poder.where(poderante: poderante).where.not(id: self.id).where('fecha_otorgamiento > ? OR (fecha_otorgamiento = ? AND id > ?)',fecha,fecha,self.id)
      if poderes_futuros.empty?
        0
      else
        3
      end
    end
  end



end
