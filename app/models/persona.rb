module NumberModelHelper
  include ActionView::Helpers::NumberHelper

  def format_number(integer)
    number_with_delimiter(integer, delimiter:'.')
  end

  def calcular_verificador(number)
    multiplicadores = [2,3,4,5,6,7]
    m_length = multiplicadores.length
    num_s = number.to_s
    num_length = num_s.length
    result = 0
    for i in (0..(num_length-1))
      index = -1 * (i+1)
      digito_acutal = num_s[num_length+index].to_i
      multiplicador = multiplicadores[i%m_length]
      result += digito_acutal*multiplicador
    end
    result = 11 - (result % 11)
    if(result == 11)
      return '0'
    elsif (result == 10)
      return 'K'
    else
      return result.to_s
    end
  end

  def rut_to_int(rut_string)
    cuerpo = rut_string.gsub('.','').gsub('-','')[0..-2].to_i
    verificador = rut_string[-1]
    v = calcular_verificador(cuerpo)
    if verificador == v
      return cuerpo
    else
      return nil
    end
  end

  # calcularVerificador = function(num){
  #   multiplicadores = [2,3,4,5,6,7];
  #   m_length = multiplicadores.length;
  #   num_s = num.toString();
  #   num_length = num_s.length;
  #   result = 0;
  #   for (i = 0; i < num_length; i++) {
  #       index = -1 * (i+1);
  #   digito_actual = parseInt(num_s.charAt(num_length+index));
  #   multiplicador = multiplicadores[i%m_length];
  #   result += digito_actual*multiplicador;
  #   }
  #   result = 11 - (result%11);
  #   if(result == 11){
  #       return '0';
  #   } else if (result == 10){
  #       return 'K';
  #   } else {
  #       return result.toString();
  #   }
  #   };
end

class Persona < ActiveRecord::Base
  include NumberModelHelper
  extend NumberModelHelper
  has_many :poderes_recibidos,  class_name: "Poder", foreign_key: "apoderado_id"
  has_many :poderes_enviados, class_name: "Poder", foreign_key: "poderante_id"
  has_many :tiempo_de_votacions
  belongs_to :votacion
  validates_uniqueness_of :rut_int, :scope => :votacion_id
  @@estados = ['Ausente', 'Asistente','Candidato']
  @@estados_voto = ['No ha votado','Está votando','Ya votó','Fuera de tiempo']
  @@tipos_voto = ['No ha votado','Electronico','Manual']

  def tipo_voto_str
    @@tipos_voto[self.tipo_voto]
  end

  def electo
      votacion = self.votacion
      n_vacantes = self.votacion.n_ganadores
      candidatos = votacion.candidatos.sort_by { |c| -1*c.votos_recibidos_totales}
      ultimo = nil
      for i in 0..(n_vacantes-1)
        ultimo = candidatos[i]
        if candidatos[i] == self
          return true
        end
      end
      if ultimo and ultimo.votos_recibidos_totales == self.votos_recibidos_totales
        return true
      end
    return false
  end

  # def self.no_han_votado
  #   arr = self.where.not(estado:0)
  #   result = []
  #   arr.each do |p|
  #     if p.estado_voto != 2
  #       result.append(p)
  #     end
  #   end
  #   r_map = result.map{|r| r.id}
  #   Persona.where(id: r_map)
  # end

  def self.no_han_votado
    self.where.not(estado: 0).where(ha_votado: false)
  end



  def estado_str
    @@estados[self.estado]
  end

  def ultima_sesion
    self.tiempo_de_votacions.last
  end

  def estado_voto
    ultima_sesion = self.ultima_sesion
    if ultima_sesion == nil
      return 0
    elsif ultima_sesion.activa && !self.ha_votado
      return 1
    elsif self.ha_votado
      return 2
    else
      return 3
    end
  end

  def estado_voto_str
    @@estados_voto[self.estado_voto]
  end

  def rut_string
    rut_i = self[:rut_int]
    if(!rut_i.nil?)
      format_number(rut_i) + '-' + calcular_verificador(rut_i)
    end
  end

  def info
    "#{self.rut_string} #{self.nombre}"
  end

  def nombre_resultado
    result = self.nombre
    if self.electo
      result += ' (ELECTO/A)'
    end
    result
  end

  def rut_string=(value)
    rut = rut_to_int(value)
    write_attribute(:rut_int,rut) unless rut.nil?
    return rut
  end

  def habilitar
    votacion = self.votacion
    cabina = votacion.get_siguiente_cabina
    self.update_attribute(:tipo_voto,1)
    tiempo_de_votacion = TiempoDeVotacion.create(persona_id: self.id, cabina: cabina, votacion_id: self.votacion.id)
    return cabina.number
  end

  def habilitar_manual
    self.update(tipo_voto:2,ha_votado: true)
    tiempo_de_votacion = TiempoDeVotacion.create(persona_id: self.id, votacion_id: self.votacion.id)
  end

  def cabina
    ultima_sesion = self.ultima_sesion
    if ultima_sesion
      return ultima_sesion.cabina
    end
  end

  def puede_ser_candidato
    dif = 8
    ingreso = self.fecha_ingreso
    anio_ingreso = ingreso.year
    mes_ingreso = ingreso.month
    hoy = Date.today
    anio_hoy = hoy.year
    mes_hoy = hoy.month
    if(!self.es_socio)
      return false
    end
    return (anio_hoy - anio_ingreso > dif + 1) || (anio_hoy - anio_ingreso  > dif && mes_hoy > mes_ingreso)
  end


  def self.pueden_ser_candidatos(relation = Persona.all)
    dif = 8
    hoy = Date.today
    anio_hoy = hoy.year
    mes_hoy = hoy.month
    relation.where('(? - extract(year  from fecha_ingreso) > ?) OR (? - extract(year  from fecha_ingreso)>? && ? > extract(month  from fecha_ingreso))', anio_hoy,dif+1,anio_hoy,dif,mes_hoy)
  end

  def self.find_by_rut_string(value)
    find_by_rut_int(rut_to_int(value))
  end

  def self.where_rut_string(value)
    where(rut_int: rut_to_int(value))
  end

  def poderes_validos_recibidos
    poderes_recibidos = self.poderes_recibidos
    poderes_validos = []
    poderes_recibidos.find_each do |pr|
      if pr.valido
        poderes_validos.append(pr)
      end
    end
    poderes_validos
  end

  def voto_propio_disponible
    if self.es_socio
      1
    else
      0
    end
  end


  def votos_totales_disponibles
    self.voto_propio_disponible + self.votos_extra_disponibles
  end


  def votos_extra_disponibles
    asiste = votacion.asiste?(self)
    return 0 unless asiste
    result = self.poderes_validos_recibidos.size
    max_representados = votacion.total_maxima_representacion
    if result > max_representados
      result = max_representados
    end
    result
  end

  def votos_recibidos_totales
    self.votos_recibidos_electronicos + self.votos_recibidos_manuales
  end



end
