require 'test_helper'

class PortalVotanteControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get vista_voto" do
    get :vista_voto
    assert_response :success
  end

  test "should get ingresar_voto" do
    get :ingresar_voto
    assert_response :success
  end

end
