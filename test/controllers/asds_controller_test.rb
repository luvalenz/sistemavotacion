require 'test_helper'

class AsdsControllerTest < ActionController::TestCase
  setup do
    @asd = asds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:asds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create asd" do
    assert_difference('Asd.count') do
      post :create, asd: { a: @asd.a, a: @asd.a }
    end

    assert_redirected_to asd_path(assigns(:asd))
  end

  test "should show asd" do
    get :show, id: @asd
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @asd
    assert_response :success
  end

  test "should update asd" do
    patch :update, id: @asd, asd: { a: @asd.a, a: @asd.a }
    assert_redirected_to asd_path(assigns(:asd))
  end

  test "should destroy asd" do
    assert_difference('Asd.count', -1) do
      delete :destroy, id: @asd
    end

    assert_redirected_to asds_path
  end
end
