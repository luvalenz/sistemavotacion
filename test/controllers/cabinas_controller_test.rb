require 'test_helper'

class CabinasControllerTest < ActionController::TestCase
  setup do
    @cabina = cabinas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cabinas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cabina" do
    assert_difference('Cabina.count') do
      post :create, cabina: { ip: @cabina.ip, number: @cabina.number, votacion_id: @cabina.votacion_id }
    end

    assert_redirected_to cabina_path(assigns(:cabina))
  end

  test "should show cabina" do
    get :show, id: @cabina
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cabina
    assert_response :success
  end

  test "should update cabina" do
    patch :update, id: @cabina, cabina: { ip: @cabina.ip, number: @cabina.number, votacion_id: @cabina.votacion_id }
    assert_redirected_to cabina_path(assigns(:cabina))
  end

  test "should destroy cabina" do
    assert_difference('Cabina.count', -1) do
      delete :destroy, id: @cabina
    end

    assert_redirected_to cabinas_path
  end
end
