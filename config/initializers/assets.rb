# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'
%w(home participacions personas poders sessions users votacions cabinas portal_votante).each do |controller|
  Rails.application.config.assets.precompile += ["#{controller}.js", "#{controller}.css"]
end

Rails.application.config.assets.precompile += ["bootstrap.min.js", "bootstrap.css", "nv.d3.css", "d3.js", "nv.d3.js","graficos.js"]

Rails.application.configure do
  config.assets.precompile += %w( .otf )
  config.assets.paths << Rails.root.join('app', 'assets', 'fonts')
end

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js.erb, application.scss.erb, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
