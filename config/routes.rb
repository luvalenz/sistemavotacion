Rails.application.routes.draw do


  get 'portal_votante/inicio'
  get 'portal_votante/ingresar_vista_voto'
  get 'portal_votante/vista_voto'
  put 'portal_votante/ingresar_voto'

  resources :cabinas

  # resources :participacions do
  #   collection do
  #     put 'crear_canidatura'
  #     put 'eliminar_candidatura'
  #   end
  # end


  resources :poders
  resources :personas do
    member do
      put 'eliminar_asistencia'
      put 'agregar_asistencia'
      put 'eliminar_candidatura'
      put 'agregar_candidatura'
      put 'habilitar_voto'
      put 'habilitar_voto_manual'
      get 'nomina'
    end
    collection do
      post 'create_collection'
    end
  end

  resources :votacions do
    member do
      post 'avanzar_estado'
      post 'retroceder_estado'
      get 'edit_poderes_extra'
      put 'update_poderes_extra'
      get 'show_asistentes'
      get 'ingreso_masivo'
      get 'vista_vocal_de_mesa'
      get 'vista_vocal_de_mesa_manual'
      get 'resultados'
      get 'ingreso_votos_manuales'
      put 'aceptar_votos_manuales'
      get 'agregar_candidato'
    end
  end
  resources :users do
    member do
      put 'activate'
      put 'unactivate'
    end
  end

  get 'home/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'sessions#new'

  # These routes will be for signup. The first renders a form in the browse, the second will
  # receive the form and create a user in our database using the data given to us by the user.
  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy'
  get '/invitado' => 'home#invitado'
  get '/home/ver_estado'


  get '/signup' => 'users#new'

  get '/permission_error' => 'home#permission_error', as: :permission_error



  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):


  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
